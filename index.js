const jsonServer = require('json-server')
const server = jsonServer.create()
const router = jsonServer.router('db.json')
const middlewares = jsonServer.defaults({ readOnly: true })

router.render = (req, res) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', '*');
  res.header('Access-Control-Request-Headers', '*');
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
  res.header('Access-Control-Request-Methods', 'GET, POST, PUT, DELETE, OPTIONS');

  if (Array.isArray(res.locals.data)) {
    res.json({
    	status: 200,
    	success: true,
      data: {
      	items: res.locals.data,
        _meta: {
          totalCount: 100,
          pageCount: 5,
          currentPage: 1,
          perPage: 20
        }
      }
    })
  } else {
    res.json({
      status: 200,
      success: true,
      data: res.locals.data
    })
  }
}

server.use(middlewares)

server.post('/v1/aspirasi/approval/:id', (req, res) => {
  res.json({
    status: 200,
    success: true
  })
})

server.post('/v1/aspirasi/likes/:id', (req, res) => {
  res.json({
    status: 200,
    success: true
  })
})

server.post('/v1/videos/likes/:id', (req, res) => {
  res.json({
    status: 200,
    success: true
  })
})

server.post('/v1/attachments', (req, res) => {
  res.json({
    status: 200,
    success: true,
    data: {
      photo_url: "http://103.122.5.71/api/storage/image/avatars/Lv9rvmvCMHt09yEbd30ggPl4cR81UJcH.jpg"
    }
  })
})

server.post('/v1/polling/vote/:id', (req, res) => {
  res.json({
    status: 200,
    success: true
  })
})

server.get('/v1/dashboards/aspirasi-counts', (req, res) => {
  res.json({
    status: 200,
    success: true,
    data: {
      "STATUS_APPROVAL_PENDING": 100,
      "STATUS_APPROVAL_REJECTED": 100,
      "STATUS_PUBLISHED": 100
    }
  })
})

server.get('/v1/dashboards/aspirasi-geo', (req, res) => {
  res.json({
    status: 200,
    success: true,
    data: [
      {
          "name": "Kota Bandung",
          "latitude": "107.560719344084",
          "longitude": "-7.36951760409741",
          "counts": 100
      },
      {
          "name": "Kota Bekasi",
          "latitude": "107.560719344084",
          "longitude": "-7.36951760409741",
          "counts": 100
      },
      {
          "name": "Kota Bogor",
          "latitude": "107.560719344084",
          "longitude": "-7.36951760409741",
          "counts": 100
      }
    ]
  })
})

server.get('/v1/dashboards/polling-latest', (req, res) => {
  res.json({
    status: 200,
    success: true,
    data: require('./json/dashboard-polling-latest.json')
  })
})

server.get('/v1/dashboards/polling-top', (req, res) => {
  res.json({
    status: 200,
    success: true,
    data: require('./json/dashboard-polling-top.json')
  })
})

server.get('/v1/dashboards/qa-top', (req, res) => {
  res.json({
    status: 200,
    success: true,
    data: require('./json/dashboard-qa-top.json')
  })
})

server.get('/v1/dashboards/users-leaderboard', (req, res) => {
  res.json({
    status: 200,
    success: true,
    data: require('./json/dashboard-leaderboard.json')
  })
})

server.get('/v1/questions/:id/comments', (req, res) => {
  const raw = require('./db.json')['question-comments']
  let comments = raw.filter(comment => comment.question_id == req.params.id)
  res.json({
    status: 200,
    success: true,
    data: {
      items: comments,
      _meta: {
        totalCount: comments.length,
        pageCount: 1,
        currentPage: 1,
        perPage: 20
      }
    }
  })
})

server.use(jsonServer.bodyParser)
server.use((req, res, next) => {
  if (req.method === 'POST') {
    req.body.created_at = 1554076800
    req.body.updated_at = 1554076800
  }
  // Continue to JSON Server router
  next()
})

server.use(jsonServer.rewriter({
  '/v1/dashboards/polling-top': '/dashboard-polling-top',
  '/v1/categories-types': '/categories-types',
  '/v1/aspirasi/me': '/aspirasi',
  '/v1/news/featured': '/news',
  '/v1/news/related': '/news',
  '/v1/news/statistics': '/news-statistics',
  '/v1/*': '/$1',
}))

server.use(router)
server.listen(3000, '0.0.0.0', () => {
  console.log('JSON Server is running')
})
